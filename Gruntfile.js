module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json"),

        sass: {
            dev: {
                options: {
                    style: "compressed",
                    sourcemap: "inline"
                },
                src: "scss/style.scss",
                dest: "dist/style.css"
            }
        },

        watch: {
            css: {
                files: ['scss/**/*.scss'],
                tasks: ['sass', 'autoprefixer']
            },
            js: {
                files: ['scripts/*.js'],
                tasks: ['uglify']
            }
        },

        autoprefixer: {
            dev: {
                options: {
                    browsers: ['last 10 versions', 'ie 8', 'ie 9'],
                    map: {
                        sourcesContent: true
                    }
                },
                src: "dist/style.css"
            }
        },

        concat: {
            dist: {
                //options: {
                  //  separator:''
                //},
                src: [
                    'libs/jquery-3.2.1.min.js',
                    'libs/popper.min.js',
                    'libs/bootstrap.min.js',
                    'libs/owl.carousel.min.js',
                    'libs/jquery-ui.js',
                    'libs/jquery.validate.min.js'
                    
                 
                ],
                dest: 'dist/libs.js'
            }
        },

        uglify: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'dist/script.min.js': 
                    [
                    'scripts/cookie.js',
                    'scripts/script.js',
                    'scripts/validate.js',
                    ]
                }
            }
        },
        
        // browserSync: {
        //     dev: {
        //         bsFiles: {
        //             src : [
        //                 '<%= path.css %>style.css',
        //                 '<%= base.deploy %>**/*.tpl',
        //                 '<%= path.js %>**/*.js'
        //             ]
        //         },
        //         options: {
        //             watchTask: true,
        //             port: 8080,
        //             server: '/'
        //         }
        //     }
        // },

    });


    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks("grunt-autoprefixer");
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask("base", ["sass", "autoprefixer", "uglify", "concat"]);
    grunt.registerTask("default", ["base", "watch"]);

};